package com.example.android.ashvudwallpaper.Common;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.example.android.ashvudwallpaper.Model.WallpaperItem;

import java.util.HashMap;
import java.util.Map;

public class  Common {
    public static final String STR_CATEGORY_BACKGROUND = "CategoryBackground";
    public static final String STR_WALLPAPER = "Wallpapers";
    public static String CATEGORY_ID_SELECTED;
    public static String CATEGORY_SELECTED;

    public static WallpaperItem select_background = new WallpaperItem();

    public static Map<String, Integer> getScreenResolution(Context context)
    {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        HashMap<String,Integer> map = new HashMap<>();
        map.put("width", width);
        map.put("height", height);
        return map;
    }
}
