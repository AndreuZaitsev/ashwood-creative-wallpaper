package com.example.android.ashvudwallpaper.Adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.android.ashvudwallpaper.Fragments.CategoryFragment;
import com.example.android.ashvudwallpaper.Fragments.DailyPopularFragment;
import com.example.android.ashvudwallpaper.Fragments.RecentFragment;
import com.example.android.ashvudwallpaper.R;

public class MyFragmentAdapter extends FragmentPagerAdapter {

    private Context context;

    public MyFragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return CategoryFragment.getInstance();
        } else if (position == 1) {
            return DailyPopularFragment.getInstance();
        } else if (position == 2) {
            return RecentFragment.getInstance();
        } else {
            return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.category_title);
            case 1:
                return context.getString(R.string.dailyPopular_title);
            case 2:
                return context.getString(R.string.recent_title);
            default:
                return " ";
        }
    }
}
