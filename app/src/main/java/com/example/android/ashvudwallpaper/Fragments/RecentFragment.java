package com.example.android.ashvudwallpaper.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.ashvudwallpaper.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecentFragment extends Fragment {


    // Private instance force to use getInstance() method.
    private static RecentFragment INSTANCE = null;

    public RecentFragment() {
        // Required empty public constructor
    }

    /*
     * We use Singleton Design Pattern.
     *
     * We need only one instance of fragment.
     * */
    public static RecentFragment getInstance(){
        if (INSTANCE == null){
            INSTANCE = new RecentFragment();
        }
        return INSTANCE;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recents, container, false);
    }

}
