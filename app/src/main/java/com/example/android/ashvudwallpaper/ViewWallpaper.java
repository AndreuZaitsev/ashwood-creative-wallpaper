package com.example.android.ashvudwallpaper;

import android.app.AlertDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.android.ashvudwallpaper.Common.Common;
import com.example.android.ashvudwallpaper.Helper.SaveImageHelper;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;

import timber.log.Timber;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ViewWallpaper extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 1000;
    FloatingActionButton floatingActionButton;
    ImageView imageView;
    RelativeLayout rootLayout;
    AlertDialog alertDialog;

    private Target target = new Target() {

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Timber.d("TEST onBitmapLoaded");
            WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
            try {
                wallpaperManager.setBitmap(bitmap);
                Snackbar.make(rootLayout, "Wallpaper was set", Snackbar.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
            Timber.d("TEST onBitmapFailed");

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            Timber.d("TEST onBitmapPrepareLoaded");

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_wallpaper_new);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        requestPermission();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Init
        rootLayout = findViewById(R.id.rootLayout);
        imageView = findViewById(R.id.imageThumb);
        floatingActionButton = findViewById(R.id.fabWallpaper);

        Picasso.get()
                .load(Common.select_background.getImageLink())
                .into(imageView);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int width = Common.getScreenResolution(getApplicationContext()).get("width");
                int height = Common.getScreenResolution(getApplicationContext()).get("height");

                Picasso.get()
                        .load(Common.select_background.getImageLink())
                        .resize(width, height)
                        .into(target);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.editor_menu:
                openEditor();
                return true;
            case R.id.download_menu:
                saveToGallery();
                return true;
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void saveToGallery() {
        Timber.d("TEST BEGIN saveToGallery()");
        if (ActivityCompat.checkSelfPermission(ViewWallpaper.this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Timber.d("TEST BEGIN if true saveToGallery()");
            Toast.makeText(ViewWallpaper.this, "You should grant permission", Toast.LENGTH_SHORT).show();
            try {
                requestPermission();
            } catch (Exception e) {
                Timber.d("TEST catch saveToGallery %s", e);
            }
        } else {
            Timber.d("TEST BEGIN else saveToGallery()");
            alertDialog = new AlertDialog.Builder(ViewWallpaper.this)
                    .setMessage("Save to gallery?")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // FIRE ZE MISSILES!
                            downloadImage(alertDialog);
                            Timber.d("TEST BEGIN else saveToGallery() OnClick OK");

                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            Timber.d("TEST BEGIN else saveToGallery()  OnClick Cancel");

                            dialog.cancel();
                        }
                    })

                    .create();
            alertDialog.show();
            Timber.d("TEST BEGIN else saveToGallery()  сreate alertdialog");


        }

    }

    private void downloadImage(AlertDialog alertDialog) {
        Timber.d("TEST BEGIN  downloadImage()");

        String fileName = UUID.randomUUID().toString() + " .jpg";
        Picasso.get()
                .load(Common.select_background.getImageLink())
                .into(new SaveImageHelper(getBaseContext(),
                        alertDialog,
                        ViewWallpaper.this.getContentResolver(),
                        fileName,
                        "Image description"));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Timber.d("TEST: Permission granted");
//                    Toast.makeText(this,"Permission Granted", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(this,"Permission Denied", Toast.LENGTH_SHORT).show();
                    Timber.d("TEST: Permission denied");
                }
                break;
        }
    }

    private void requestPermission() {
        if ((ActivityCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)) {
            requestPermissions(new String[]{
                    WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE
            }, PERMISSION_REQUEST_CODE);
        } else {
            Timber.d("TEST else requestPermission");
        }
    }

    public void openEditor() {
        Timber.d("TEST BEGIN openEditor()");
        if (ActivityCompat.checkSelfPermission(ViewWallpaper.this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Timber.d("TEST BEGIN if true openEditor()");
            Toast.makeText(ViewWallpaper.this, "You should grant permission", Toast.LENGTH_SHORT).show();
            try {
                requestPermission();
            } catch (Exception e) {
                Timber.d("TEST catch openEditor %s", e);

            }
        } else {
            Uri uri = getImageUri(this, convertImageViewToBitmap(imageView));
            Intent editIntent = new Intent(Intent.ACTION_EDIT);
            editIntent.setDataAndType(uri, "image/*");
            editIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(editIntent, null));
        }
    }

    private Bitmap convertImageViewToBitmap(ImageView v) {
        return ((BitmapDrawable) v.getDrawable()).getBitmap();
    }

    private Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
