package com.example.android.ashvudwallpaper.Model;

public class WallpaperItem {
    public String imageLink;
    public String categoryId;

    public WallpaperItem() {
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}
