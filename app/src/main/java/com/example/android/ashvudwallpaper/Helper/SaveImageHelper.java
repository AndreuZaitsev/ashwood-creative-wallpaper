package com.example.android.ashvudwallpaper.Helper;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.lang.ref.WeakReference;

public class SaveImageHelper implements Target {
    private Context context;
    private WeakReference<AlertDialog> alertDialogWeakReference;
    private WeakReference<ContentResolver> contentResolverWeakReference;
    private String name;
    private String desc;

    public SaveImageHelper(Context context, AlertDialog alertDialogWeakReference, ContentResolver contentResolverWeakReference, String name, String desc) {
        this.context = context;
        this.alertDialogWeakReference = new WeakReference<>(alertDialogWeakReference);
        this.contentResolverWeakReference = new WeakReference<>(contentResolverWeakReference);
        this.name = name;
        this.desc = desc;
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        ContentResolver r = contentResolverWeakReference.get();
        if (r != null){
            MediaStore.Images.Media.insertImage(r,bitmap,name,desc);
        }

        // Open gallery after download
       // Intent intent = new Intent();
       // //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       // intent.setType("image/*");
//
       // intent.setAction(Intent.ACTION_GET_CONTENT);
       // context.startActivity(Intent.createChooser(intent,"VIEW PICTURE"));
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        Intent chooser = Intent.createChooser(intent, "View");
        chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(chooser);

    }

    @Override
    public void onBitmapFailed(Exception e, Drawable errorDrawable) {

    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {

    }
}
