package com.example.android.ashvudwallpaper.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.ashvudwallpaper.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DailyPopularFragment extends Fragment {


    // Private instance force to use getInstance() method.
    private static DailyPopularFragment INSTANCE = null;

    public DailyPopularFragment() {
        // Required empty public constructor
    }

    /*
     * We use Singleton Design Pattern.
     *
     * We need only one instance of fragment.
     * */
    public static DailyPopularFragment getInstance(){
        if (INSTANCE == null){
            INSTANCE = new DailyPopularFragment();
        }
        return INSTANCE;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_daily_popular, container, false);
    }

}
